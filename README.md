## Deftouch Task

**src** folder contains source code.

**bin** folder contains executables.

--------------------------------------------------------------------

Executable Instructions

1. Launch redis server in **localhost:6379** with default DB **0** and with **no password** for the db
2. Execute **server2_go.exe**
3. Execute **server1_go.exe**
4. Execute **client_go.exe**