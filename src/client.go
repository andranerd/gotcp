package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

func main() {
	var message string
retry:
	fmt.Print("Type your message: ")
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		message = scanner.Text()
	}

	// Retrying if there a empty message
	if len(message) <=0 {
		fmt.Println("Invalid Input.. Please Try Again..")
		goto retry
	}
	//Connecting to Server 1
	conn, err := net.Dial("tcp", "localhost:5000")
	if err != nil {
		log.Println("Server 1 Dial error:", err)
		os.Exit(1)
	}

	//Closing the connection
	defer conn.Close()
	//Sending data to Connection
	fmt.Fprintf(conn, message)

	//Reading the response from Server 1
	var buf bytes.Buffer
	io.Copy(&buf, conn)
	fmt.Println(buf.String())
	fmt.Println("Press any key to Exit..")
	fmt.Scanln()
}