package main

import (
	"bytes"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/sony/sonyflake"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

const (
	SERVER1_CONN_HOST = "localhost"
	SERVER1_CONN_PORT = "5000"
	SERVER1_CONN_TYPE = "tcp"
)

// Handles incoming requests.
func handleServer1Request(conn net.Conn) {

	// Closing the  connection
	defer conn.Close()

	// Making buffer to hold incoming data.
	buf := make([]byte, 1024)

	// Read the incoming connection into the buffer.
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading message from client:", err.Error())
		return
	}


	// Creating a Unique Key to store the data
	st := sonyflake.Settings{}
	generator := sonyflake.NewSonyflake(st)
	key,err := generator.NextID()

	if err != nil {
		fmt.Println("Error in generating Unique key..", err.Error())
		return
	}

	//Write data to redis
	writeToRedis(string(buf[:n]), key)

	// Forwarding to Server 2
	fmt.Println("Request from client. Forwarding to Server 2")

	response, err := contactServer2(string(key))
	if err != nil {
		conn.Write([]byte("Message from Server 1:"+err.Error()))
		return
	}
	conn.Write([]byte(response.String()))

}

func contactServer2(redisKey string) (bytes.Buffer ,error) {

	//Establishing a connection to Server 2
	conn, err := net.Dial("tcp", "localhost:7000")
	if err != nil {
		fmt.Println("Server 2 Dial Error:", err)
		return bytes.Buffer{}, err
	}

	//Closing the connection to Server 2
	defer conn.Close()

	//Sending date to server 2
	fmt.Fprintf(conn, redisKey)
	var buf bytes.Buffer
	io.Copy(&buf, conn)
	return buf,nil
}

func writeToRedis(message string, key uint64){

	//Connecting to Redis Server
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	//Closing redis connection
	defer  client.Close()

	fmt.Println("Writing Data to Redis")

	message = strings.TrimSpace(message)
	//Writing data to redis
	_, err := client.Set(string(key), message, 0).Result()
	if err != nil {
		fmt.Println("Failed to write data to Redis")
		return
	}
}

func main()  {

	//Creating a Connection - Server 1
	l, err := net.Listen(SERVER1_CONN_TYPE, SERVER1_CONN_HOST+":"+SERVER1_CONN_PORT)
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}

	//Closing the connection
	defer l.Close()

	fmt.Println("Server 1: Listening on " + SERVER1_CONN_HOST + ":" + SERVER1_CONN_PORT)

	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			log.Println("Server 1: Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleServer1Request(conn)
	}
}
