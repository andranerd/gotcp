package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"log"
	"net"
	"os"
)

const (
	SERVER2_CONN_HOST = "localhost"
	SERVER2_CONN_PORT = "7000"
	SERVER2_CONN_TYPE = "tcp"
)

// Handles incoming requests.
func handleServer2Request(conn net.Conn) {
	// Close the connection when you're do ne with it.
	defer conn.Close()

	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	// Read the incoming connection into the buffer.
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}

	//Read data from redis
	message := readDataFromRedis(string(buf[:n]))

	// Send a response back to person contacting us.
	fmt.Println("Got a Request from Server 1")
	conn.Write([]byte("Response from server 2 through 1: "+message))
}

func readDataFromRedis(key string) string {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer  client.Close()

	//Reading from Redis
	message, err := client.Get(key).Result()
	if err != nil {
		return err.Error()
	}
	return message
}

func main()  {

	//Creating a Connection - Server 2
	l, err := net.Listen(SERVER2_CONN_TYPE, SERVER2_CONN_HOST+":"+SERVER2_CONN_PORT)
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}

	//Closing the connection
	defer l.Close()

	fmt.Println("Server 2 Listening on " + SERVER2_CONN_HOST + ":" + SERVER2_CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			log.Println("Server 2 : Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleServer2Request(conn)
	}
}

